// Here's an example of a Rust function that takes a JSON array as an input parameter and saves it to a MySQL database using the mysql crate:
// Chat GPT helped me)
use mysql::prelude::*;
use mysql::*;
// use serde_json::Value;

fn connect_to_db() {}

// fn save_to_db(json_array: Vec<Value>) -> Result<(), Box<dyn std::error::Error>> {
pub fn save_to_db() -> Result<(), Box<dyn std::error::Error>> {
    let url = "mysql://sysinfo:234werWER@127.0.0.1:3307/sysinfo";
    let pool = Pool::new(url)?;

    let mut conn = pool.get_conn()?;
    conn.exec_drop(
        "INSERT INTO logons (userlogin, username) VALUES (?)",
        ("user1", "user name 1"),
    )?;

    // for json_value in json_array {
    //     let json_str = json_value.to_string();

    //     conn.exec_drop("INSERT INTO table_name (json_data) VALUES (?)", (json_str,))?;
    // }

    Ok(())
}

// In this function:
// - Replace username, password, localhost, and database_name with your MySQL database connection details.
// - Replace table_name with the name of the table where you want to save the JSON data.
// - The function takes a vector of serde_json::Value objects as input, representing the JSON array.
// - It establishes a connection to the MySQL database, iterates over the JSON array, converts each JSON value to a string, and inserts it into the database table.

// Make sure to add the necessary dependencies (mysql and serde_json) to your Cargo.toml file. Let me know if you need any further assistance!
